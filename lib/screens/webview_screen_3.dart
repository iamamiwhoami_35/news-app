import 'package:flutter/material.dart';
import 'package:news_app/common/theme.dart';
import 'package:webview_flutter/webview_flutter.dart';

class JoinMailList extends StatefulWidget {
  final String url =
      'https://manage.kmail-lists.com/subscriptions/subscribe?a=WkuMfY&g=VzKJMJ';
  _JoinMailListState createState() => _JoinMailListState(url);
}

class _JoinMailListState extends State<JoinMailList> {
  var _url;
  final _key = UniqueKey();
  bool _isLoading = true;
  _JoinMailListState(
    this._url,
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemesData.BACKGROUND_COLOR,
      appBar: AppBar(
        title: Text(
          'Join Mailing List',
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
        ),
        backgroundColor: Colors.transparent,
        toolbarHeight: 88,
      ),
      body: Stack(
        children: [
          Container(
            alignment: Alignment.center,
            child: WebView(
              key: _key,
              javascriptMode: JavascriptMode.unrestricted,
              initialUrl: _url,
              onPageFinished: (finish) {
                setState(() {
                  _isLoading = false;
                });
              },
            ),
          ),
          _isLoading
              ? Container(
                  color: ThemesData.BACKGROUND_COLOR,
                  child: Center(
                    child: CircularProgressIndicator(
                        //color: Color(0xFFFC7C54),
                        ),
                  ),
                )
              : Stack(),
        ],
      ),
    );
  }
}
